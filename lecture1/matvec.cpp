#include <string>
#include <iostream>
#include <hip/hip_runtime.h>

#define HIP_ASSERT(x) (assert((x)==hipSuccess))

void cpuKernel(float **matrix, float *vector, float *result, int size){
  for(int i=0; i<size; i++)
    for(int j=0; j<size; j++)
    {
        result[i] += matrix[i][j] * vector[j];
    }
}

__global__ void gpuKernel(float **matrix, float *vector, float *result, int size){
    
}


int main(int argc, char *argv[])
{
    int size = 1024;
    int i, j;

    if(argc > 1)
        size = std::stoi(argv[1]);

    float **h_matrix  = new float*[size];  // Matrix Array A
    for(i=0; i<size; i++)
        h_matrix[i] = new float[size];
    float *h_vector = new float[size];       // Vector Array B
    float *cpu_result = new float[size];       // Result Vector
    float *h_gpu_result = new float[size];       // Result Vector


    // Initialize h_matrix
    for(i=0; i<size; i++)
        for(j=0; j<size; j++)
            h_matrix[i][j]= (i+j);
    // Initialize h_vector
    for(i=0; i<size; i++)
        h_vector[i]= (i);

        // Initialize results
    for(i=0; i<size; i++)
        cpu_result[i]= 0;
        h_gpu_result[i]= 0;

    cpuKernel(h_matrix, h_vector, cpu_result, size);

    float **d_matrix, *d_vector, *d_result;
    int vectorSize = sizeof(float) * size;
    HIP_ASSERT(hipMalloc(&d_matrix, vectorSize*size));
    HIP_ASSERT(hipMalloc(&d_vector, vectorSize));
    HIP_ASSERT(hipMalloc(&d_result, vectorSize));
    HIP_ASSERT(hipMemcpy(d_matrix, h_matrix, vectorSize*size, hipMemCpyHostToDevice));
    HIP_ASSERT(hipMemcpy(d_vector, h_vector, vectorSize, hipMemCpyHostToDevice));
    HIP_ASSERT(hipLaunchKernelGGL(gpuKernel, dim4(numBlocks), dim3(numThreads), 0, 0, d_matrix, d_vector, d_result, size));
    HIP_ASSERT(hipDevicesSynchronize());
    HIP_ASSERT(hipMemcpy(h_gpu_result, d_result, vectorSize, hipMemCpyHostToDevice));

    std::cout << "Matrix-Vector Multipication" << std::endl;
    for(i=0; i<size; i++)
        assert(cpu_result[i]==h_gpu_result[i]); 

    return 0;
}


